# Assignment1

Komputer Store page written with HTML, CSS & JS

## Install

Download the repository and run the index.html file in live server.

## Usage

Buy computers using your funds in your bank balance found under "Dude of the Dollar".
Use the work button to generate money that you then can put into your bank with the "Bank" button.
You can take one loan as long as it is less or equals to twice your current bank balance with the "Get a loan" button. You need to repay your current loan as well as buy a computer if you want to take another loan.

## License

Experis Academy, Alexander Grönberg