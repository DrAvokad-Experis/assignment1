//Bank Section
const bankBalance = document.getElementById("bankBalance");
const getLoanButton = document.getElementById("getLoanButton");
const currentLoan = document.getElementById("currentLoan");

//Work Section
const payTotal = document.getElementById("payTotal");
const bankButton = document.getElementById("bankButton");
const workButton = document.getElementById("workButton");
const payLoanButton = document.getElementById("payLoanButton");

//Laptop Select Section
const laptopSelect = document.getElementById("laptopSelect");
const laptopFeatures = document.getElementById("laptopFeatures");

//Laptop Info Section
const laptopImage = document.getElementById("laptopImage")
const laptopName = document.getElementById("laptopName");
const laptopPrice = document.getElementById("laptopPrice");
const laptopDescription = document.getElementById("laptopDescription");
const buyButton = document.getElementById("buyButton");

let outstandingLoan = 0;

let activeLoan = false;
let loanApproved = true;

let computers = [];
let currentComputer = null;

//Returns true if the number is less or equal to dubble its own value
const loanCheck = (number) => {
    return (number <= (2*getCurrentBalance()))
}

//Updates the bank balance in the DOM with the given amount if it is less or 
//equal than the amount input and another loan has not already been taken.
const loan = (amount) => {
    if(activeLoan){
        alert("You cannot take another loan before you have paid back your first!")
    }else if(!loanApproved){
        alert("You are not approved for another loan until you have purchased a computer!")
    }
    else if(loanCheck(amount) && loanApproved){
        let currentBalance = getCurrentBalance();
        activeLoan = true;
        loanApproved = false;
        outstandingLoan = amount;
        currentBalance += amount;
        document.getElementById("bankBalance").innerText=currentBalance;
        document.getElementById("currentLoan").innerText=`Outstanding Loan: ${outstandingLoan} sek`;
        payLoanButton.style.display='block';
    }else{
        alert("You cannot take a loan greater than dubble your current balance!")
    }
}

//Promts the user to enter the amount they wish to loan.
const loanPrompt = () => {
    const loanAmount = prompt("Please enter the loan amount: ");
    const loanNumber = parseInt(loanAmount);
    loan(loanNumber);
}

//Adds 100 to the total acumulated pay
const work = () => {
    let pay = getCurrentPay();
    pay += 100;
    document.getElementById("payTotal").innerText=pay;
}

//Adds the acumulated pay to the bank balance. If there is an outstanding loan,
//10% of the acumulated pay is used to pay back the loan. If the 10% manages to
//pay back the loan in full, the surplus is added to the bank balance.
const bank = () => {
    let currentAmount = getCurrentPay();
    let finalAmount = 0;
    if(activeLoan){
        let activeLoanAmount = getCurrentLoan();
        let rent = currentAmount*0.10;
        let surplus = 0;
        if(rent > activeLoanAmount){
            surplus = Math.abs(activeLoanAmount-rent);
            activeLoan = false;
            document.getElementById("currentLoan").innerText="";
            payLoanButton.style.display='none'
            finalAmount = currentAmount + surplus - rent;
        }else{
            outstandingLoan = activeLoanAmount-rent;
            document.getElementById("currentLoan").innerText=`Outstanding Loan: ${(outstandingLoan)} sek`;
            finalAmount = currentAmount - rent;
        }

    }else{
        finalAmount = currentAmount
        
    }

    finalAmount += getCurrentBalance();
    document.getElementById("bankBalance").innerText=finalAmount;
    document.getElementById("payTotal").innerText=0;
}

//Subtracts the current amount displayed in current pay from the current loan.
//If the result is a negative number the result is added as an absolute value
//to the bank blance and the active loan boolean is set to false.
const payLoan = () => {
    const currentLoan = getCurrentLoan();
    const currentPay = getCurrentPay();
    let total = 0;
    if(activeLoan === true){
        if(currentLoan <= currentPay){
            const surplus = Math.abs(currentLoan-currentPay);
            total = parseInt(getCurrentBalance() + surplus);
            document.getElementById("bankBalance").innerText=total;
            payLoanButton.style.display='none'
            document.getElementById("currentLoan").innerText="";
            activeLoan=false;
        }else{
            outstandingLoan = currentLoan - currentPay;
            document.getElementById("currentLoan").innerText=`Outstanding Loan: ${outstandingLoan} sek`;
        }
        document.getElementById("payTotal").innerText=0;
    }
    
}

//get
const getCurrentBalance = () => {
    return parseInt(bankBalance.innerHTML);
}

const getCurrentPay = () => {
    return parseInt(payTotal.innerHTML);
}

const getCurrentLoan = () => {
    return parseInt(outstandingLoan);
}

const getComputerPrice = () => {
    return parseInt(laptopPrice.innerHTML);
}


//Fetches JSON data from the computers API then calls populateLaptopSelect with
// the fetched data
async function getComputerData(){
    try {
        await fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
        .then(response => response.json())
        .then(data => computers = data)
        .then(computers => populateLaptopSelect(computers))
    } catch (e) {
        console.error(e.name + ': ' + e.message)
    }
}

//Populate laptopSelect
const populateLaptopSelect = (computers) => {
    computers.forEach(x => addComputer(x));
    setComputerSelection(computers[0]);
    setComputerName(computers[0]);
    setComputerDescription(computers[0]);
    setComputerPrice(computers[0]);
    setLaptopImage(computers[0]);
    currentComputer = computers[0];
}

//Adds a computer option to the laptop select.
const addComputer = (computer) => {
    const computerOption = document.createElement("option");
    computerOption.value = computer.id;
    computerOption.appendChild(document.createTextNode(computer.title));
    document.getElementById("laptopSelect").appendChild(computerOption);
}

getComputerData();


//Extracts the specs of a computer from the list of computers from the JSON
//document depending on what index is given. The specs is then displayed in
//the DOM.
const setComputerSelection = (computer) => {
    let comp = computer;
    let features = ""
    for (spec of comp.specs){
        features += spec + "\n"
    }
    document.getElementById("laptopFeatures").innerText=features
}

//Set laptop Name
const setComputerName = (computer) => {
    document.getElementById("laptopName").innerText=computer.title;
}

//Set laptop Description
const setComputerDescription = (computer) => {
    document.getElementById("laptopDescription").innerText=computer.description;
}

//Set laptop Price
const setComputerPrice = (computer) => {
    document.getElementById("laptopPrice").innerText=`${computer.price} sek`;
}

//Set laptop Image
const setLaptopImage = (computer) => {
    document.getElementById("laptopImage").src=`https://noroff-komputer-store-api.herokuapp.com/${computer.image}`;
}

//Checks for sufficient funds in the bank and if sufficient, buys the currently
//selected computer.
const buyComputer = () => {
    console.log(getComputerPrice());
    console.log(getCurrentBalance());

    if(getCurrentBalance() >= getComputerPrice()){
        let newBalance = getCurrentBalance() - getComputerPrice();
        document.getElementById("bankBalance").innerText=newBalance;
        loanApproved = true;
        alert(`Congratulations on your purchase of your new ${currentComputer.title}!`)
        addComputerToList(currentComputer);
    }else{
        alert("You lack sufficient funds to buy this computer!");
    }
}

//Add
const addComputerToList = (computer) => {
    const purchasedComputer = document.createElement("li");
    purchasedComputer.innerText = `${computer.title}`
    document.getElementById("purchasedComputers").appendChild(purchasedComputer);
}

//Event handlers

//Handle computer selection
const handleComputerSelection = e => {
    const selectedComputer = computers[e.target.selectedIndex];
    currentComputer = selectedComputer;
    setComputerSelection(selectedComputer);
    setComputerName(selectedComputer);
    setComputerDescription(selectedComputer);
    setComputerPrice(selectedComputer);
    setLaptopImage(selectedComputer);
}

//Listeners
getLoanButton.addEventListener("click", loanPrompt);
workButton.addEventListener("click", work);
bankButton.addEventListener("click", bank);
payLoanButton.addEventListener("click", payLoan);
buyButton.addEventListener("click", buyComputer);
laptopSelect.addEventListener("change", handleComputerSelection);











//test functions


